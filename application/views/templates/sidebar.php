<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/')?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>M. Syafiq Roikhan M.</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="active treeview menu-open">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-university"></i>
            <span>Beasiswa</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Informasi</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Status</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-graduation-cap"></i>
            <span>Prestasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Tambahkan Prestasi</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Cetak Rekap Prestasi</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-envelope"></i>
            <span>Permohonan Surat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Pengajuan Surat</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Status</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Evaluasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Praktek Kerja Lapangan(PKL)</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Prektek Kegiatan Mengajar(PKM)</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Kegiatan TA/Skripsi</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Layanan Kemahasiswaan</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Cetak Evaluasi</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Akun Saya</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Ubah Biodata</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Ubah Password</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Log Out</a></li>
          </ul>
        </li>
        
    </section>
    <!-- /.sidebar -->
  </aside>