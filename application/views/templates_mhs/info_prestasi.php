<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Informasi Prestasi
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">UI</a></li>
        <li class="active">General</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- START ALERTS AND CALLOUTS -->
        <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-bullhorn"></i>
              <h3 class="box-title">Pentingnya Mengunggah dan Memperbaharui Prestasi Anda</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="callout callout-info">
                <h4>Tata Cara Mengunggah dan Memperbaharui Prestasi</h4>
                <ol>
                <li>BLABLABLAAA</li>
                <li>BLABLABLAAA</li>
                <li>BLABLABLAAA</li>
                <li>BLABLABLAAA</li>
                <li>BLABLABLAAA</li>
                <li>BLABLABLAAA</li>
                </ol>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      <!-- /.row -->
      <!-- END ALERTS AND CALLOUTS -->