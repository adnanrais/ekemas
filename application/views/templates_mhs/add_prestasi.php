<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Tambah Prestasi
			<small>FT UNJ</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Prestasi</a></li>
			<li class="active">Tambah Prestasi</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Horizontal Form -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Formulir Pengisian Prestasi</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form class="form-horizontal">
				<div class="box-body">
					<div class="form-group">
						<label for="inputEmail" class="col-sm-4 control-label">Nama Lengkap</label>

						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputEmail" placeholder="Nama Lengkap">
						</div>
					</div>
					<div class="form-group">
						<label for="inputnim" class="col-sm-4 control-label">NIM</label>

						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputnim" placeholder="NIM">
						</div>
					</div>
					<div class="form-group">
						<label for="inputprodi" class="col-sm-4 control-label">Program Studi</label>

						<div class="col-sm-8">
								<select class="form-control select2">
									<option selected="selected">--Pilih Program Studi--</option>
									<option>S1 Pendidikan Teknik Informatika dan Komputer</option>
									<option>S1 Pendidikan Teknik Elektro</option>
									<option>S1 Pendidikan Teknik Elektronika</option>
									<option>S1 Pendidikan Teknik Mesin</option>
									<option>S1 Pendidikan Teknik Bangunan</option>
									<option>S1 Pendidikan Tata Boga</option>
									<option>S1 Pendidikan Tata Rias</option>
									<option>S1 Pendidikan Tata Busana</option>
									<option>S1 Pendidikan Kesejahteraan Keluarga</option>
									<option>D3 Teknik Elektronika</option>
									<option>D3 Teknik Mesin</option>
									<option>D3 Teknik Sipil</option>
									<option>D3 Transportasi</option>							
									<option>D3 Tata Rias</option>							
									<option>D3 Tata Busana</option>							
									<option>D3 Tata Boga</option>							
								</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputsemester" class="col-sm-4 control-label">Semester</label>

						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputsemester" placeholder="Semester">
						</div>
					</div>
					<div class="form-group">
						<label for="inputkategori" class="col-sm-4 control-label">Kategori Prestasi</label>

						<div class="col-sm-8">
								<select class="form-control select2">
									<option selected="selected">--Pilih Kategori Prestasi--</option>
									<option>Lomba</option>
									<option>Non Lomba</option>							
								</select>
						</div>
					</div>
					