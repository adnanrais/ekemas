<div class="form-group">
	<label for="inputnmkeg" class="col-sm-4 control-label">Nama Kegiatan Kebangsaan</label>

	<div class="col-sm-8">
		<select class="form-control select2">
			<option selected="selected">--Pilih Nama Kegiatan Kebangsaan--</option>
			<option>Pelatihan Kepemimpinan Mahasiswa</option>
			<option>Pendidikan Bela Negara/Kewiraan/Wawasan Nusantara</option>
			<option>Pendidikan Norma, Etika, Pembinaan Karakter</option>
			<option>Pendidikan atau Gerakan Anti Korupsi</option>
			<option>Pendidikan atau Gerakan Anti Penyalahgunaan NAPZA</option>
			<option>Pendidikan atau Gerakan Anti Radikalisme</option>
			<option>Kampanye Pencegahan Kekerasan Seksual</option>
			<option>Kampanye Kampus Sehat/Green Kampus</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="inputtgkt" class="col-sm-4 control-label">Tingkat Penyelenggaraan</label>

	<div class="col-sm-8">
		<select class="form-control select2">
			<option selected="selected">--Pilih Tingkat Penyelenggaraan--</option>
			<option>Tingkat Perguruan Tinggi</option>
			<option>Tingkat Fakultas</option>
			<option>Tingkat Program Studi</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="inputjudul" class="col-sm-4 control-label">Judul Kegiatan</label>

	<div class="col-sm-8">
		<input type="text" class="form-control" id="inputjudul" placeholder="Judul Kegiatan">
	</div>
</div>
<div class="form-group">
	<label for="inputwaktu" class="col-sm-4 control-label">Waktu Penyelenggaraan</label>

	<div class="col-sm-8">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input type="text" class="form-control pull-right" id="datepicker">
		</div>
	</div>
</div>
<div class="form-group">
	<label for="inputtempat" class="col-sm-4 control-label">Tempat Penyelenggaraan</label>

	<div class="col-sm-8">
		<input type="text" class="form-control" id="inputtempat" placeholder="Tempat Penyelenggaraan">
	</div>
</div>
<div class="box-header with-border">
	<h3 class="box-title">Lampiran Berkas Pendukung</h3>
</div>
<div class="form-group">
    <label for="inputpengesahan" class="col-sm-4 control-label">Scan Surat Tugas/Lembar Pengesahan</label>

    <div class="col-sm-8">
        <input type="file" id="inputpengesahan">
    </div>
</div>
<div class="form-group">
    <label for="inputfoto" class="col-sm-4 control-label">Foto Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputfoto">
    </div>
</div>
<div class="form-group">
    <label for="inputsertif" class="col-sm-4 control-label">Sertifikat Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputsertif">
    </div>
</div>
<div class="form-group">
    <label for="inputlaporan" class="col-sm-4 control-label">Laporan Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputlaporan">
    </div>
</div>
</div>
<!-- /.box-body -->
<div class="box-footer">
	<button type="submit" class="btn btn-info pull-left" style="margin-left:35px">Simpan</button>
</div>
<!-- /.box-footer -->
</form>
</div>
<!-- /.box -->
</section>
<!-- /.content -->
</div>
