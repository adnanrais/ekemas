<div class="form-group">
	<label for="inputtingkat" class="col-sm-4 control-label">Tingkat </label>

	<div class="col-sm-8">
		<select class="form-control select2">
			<option selected="selected">--Pilih Tingkat --</option>
			<option>Regional Wilayah</option>
			<option>Nasional</option>
			<option>Internasional</option>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="inputnmlomba" class="col-sm-4 control-label">Nama Lomba</label>

	<div class="col-sm-8">
		<input type="text" class="form-control" id="inputnmlomba" placeholder="Nama Lomba">
	</div>
</div>
<div class="form-group">
	<label for="inputpenyelenggara" class="col-sm-4 control-label">Penyelenggara</label>

	<div class="col-sm-8">
		<input type="text" class="form-control" id="inputpenyelenggara" placeholder="Penyelenggara">
	</div>
</div>
<div class="form-group">
	<label for="inputurl" class="col-sm-4 control-label">URL Penyelenggara</label>

	<div class="col-sm-8">
		<input type="text" class="form-control" id="inputurl" placeholder="Penyelenggara">
	</div>
</div>
<div class="form-group">
	<label for="inputwaktu" class="col-sm-4 control-label">Waktu Penyelenggaraan</label>

	<div class="col-sm-8">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input type="text" class="form-control pull-right" id="datepicker">
		</div>
	</div>
</div>
<div class="form-group">
	<label for="inputcapaian" class="col-sm-4 control-label">Capaian Prestasi </label>

	<div class="col-sm-8">
		<select class="form-control select2">
			<option selected="selected">--Pilih Capaian Prestasi --</option>
			<option>Juara Umum</option>
			<option>Juara I</option>
			<option>Juara II</option>
			<option>Juara III</option>
			<option>Harapan</option>
			<option>Partisipasi/Delegasi/Peserta</option>
			<option>Penyelenggara</option>
			<option>Apresiasi</option>
		</select>
	</div>
</div>
