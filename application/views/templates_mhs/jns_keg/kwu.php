<div class="form-group">
    <label for="inputnmprogram" class="col-sm-4 control-label">Nama Program</label>

    <div class="col-sm-8">
        <input type="text" class="form-control" id="inputnmprogram" placeholder="Nama Program">
    </div>
</div>
<div class="form-group">
	<label for="inputkegiatan" class="col-sm-4 control-label">Jumlah Mahasiswa Yang Terlibat</label>

	<div class="col-sm-8">
		<select class="form-control select2">
			<option selected="selected">--Jumlah Mahasiswa Yang Terlibat--</option>
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option>5</option>
		</select>
	</div>
</div>
<div class="form-group">
    <label for="inputdospem" class="col-sm-4 control-label">Dosen Pembimbing</label>

    <div class="col-sm-8">
        <input type="text" class="form-control" id="inputdospem" placeholder="Dosen Pembimbing">
    </div>
</div>
<div class="box-header with-border">
    <h3 class="box-title">Lampiran Berkas Pendukung</h3>
</div>
<div class="form-group">
    <label for="inputpengesahan" class="col-sm-4 control-label">Scan Surat Tugas/Lembar Pengesahan</label>

    <div class="col-sm-8">
        <input type="file" id="inputpengesahan">
    </div>
</div>
<div class="form-group">
    <label for="inputfoto" class="col-sm-4 control-label">Foto Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputfoto">
    </div>
</div>
<div class="form-group">
    <label for="inputsertif" class="col-sm-4 control-label">Sertifikat Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputsertif">
    </div>
</div>
<div class="form-group">
    <label for="inputlaporan" class="col-sm-4 control-label">Laporan Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputlaporan">
    </div>
</div>
</div>
<!-- /.box-body -->
<div class="box-footer">
	<button type="submit" class="btn btn-info pull-left" style="margin-left:35px">Simpan</button>
</div>
<!-- /.box-footer -->
</form>
</div>
<!-- /.box -->
</section>
<!-- /.content -->
</div>