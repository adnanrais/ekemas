<div class="form-group">
    <label for="inputnmprogram" class="col-sm-4 control-label">Nama Program Recognisi</label>

    <div class="col-sm-8">
		<select class="form-control select2">
			<option selected="selected">--Pilih Nama Program Recognisi--</option>
			<option>Pendaftaran Paten</option>
			<option>Hak Cipta/Buku</option>
			<option>Juri/Pelatih Internasional</option>
			<option>Juri/Pelatih Nasional</option>
			<option>Conference/Seminar Internasional</option>
			<option>Conference/Seminar Nasional</option>
			<option>Peserta Pameran Internasional</option>
			<option>Peserta Pameran Nasional</option>
		</select>
	</div>
</div>
<div class="form-group">
    <label for="inputprogram" class="col-sm-4 control-label">Judul Program Recognisi</label>

    <div class="col-sm-8">
        <input type="text" class="form-control" id="inputprogram" placeholder="Judul Program Recognisi">
    </div>
</div>
<div class="form-group">
    <label for="inputpenyelenggara" class="col-sm-4 control-label">Penyelenggara</label>

    <div class="col-sm-8">
        <input type="text" class="form-control" id="inputpenyelenggara" placeholder="Penyelenggara">
    </div>
</div>
<div class="form-group">
    <label for="inputurl" class="col-sm-4 control-label">URL Penyelenggara</label>

    <div class="col-sm-8">
        <input type="text" class="form-control" id="inputpenyelenggara" placeholder="URL Penyelenggara">
    </div>
</div>
<div class="box-header with-border">
    <h3 class="box-title">Lampiran Berkas Pendukung</h3>
</div>
<div class="form-group">
    <label for="inputsertif" class="col-sm-4 control-label">Sertifikat Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputsertif">
    </div>
</div>
<div class="form-group">
    <label for="inputdok" class="col-sm-4 control-label">Cover Buku/Dokumen</label>

    <div class="col-sm-8">
        <input type="file" id="inputdok">
    </div>
</div>
<div class="form-group">
    <label for="inputktm" class="col-sm-4 control-label">Scan Kartu Tanda Mahasiswa</label>

    <div class="col-sm-8">
        <input type="file" id="inputktm">
    </div>
</div>
<div class="form-group">
    <label for="inputisbn" class="col-sm-4 control-label">Scan No. ISBN</label>

    <div class="col-sm-8">
        <input type="file" id="inputisbn">
    </div>
</div>
<div class="form-group">
    <label for="inputisbn" class="col-sm-4 control-label">Scan Surat Undangan/Invitasi/Surat Tugas</label>

    <div class="col-sm-8">
        <input type="file" id="inputisbn">
    </div>
</div>
</div>
<!-- /.box-body -->
<div class="box-footer">
	<button type="submit" class="btn btn-info pull-left" style="margin-left:35px">Simpan</button>
</div>
<!-- /.box-footer -->
</form>
</div>
<!-- /.box -->
</section>
<!-- /.content -->
</div>