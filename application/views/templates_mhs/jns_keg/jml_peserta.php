<div class="form-group">
	<label for="inputcapaian" class="col-sm-4 control-label">Jumlah Peserta </label>

	<div class="col-sm-8">
		<select class="form-control select2">
			<option selected="selected">--Pilih Jumlah Peserta --</option>
			<option>Lebih dari 20 Perguruan Tinggi</option>
			<option>Mulai 11 Sampai 20 Perguruan Tinggi</option>
			<option>Kurang dari 11 Perguruan Tinggi</option>
		</select>
	</div>
</div>
<div class="box-header with-border">
	<h3 class="box-title">Lampiran Berkas Pendukung</h3>
</div>
<div class="form-group">
    <label for="inputpengesahan" class="col-sm-4 control-label">Scan Surat Tugas/Lembar Pengesahan</label>

    <div class="col-sm-8">
        <input type="file" id="inputpengesahan">
    </div>
</div>
<div class="form-group">
    <label for="inputfoto" class="col-sm-4 control-label">Foto Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputfoto">
    </div>
</div>
<div class="form-group">
    <label for="inputsertif" class="col-sm-4 control-label">Sertifikat Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputsertif">
    </div>
</div>
<div class="form-group">
    <label for="inputlaporan" class="col-sm-4 control-label">Laporan Kegiatan</label>

    <div class="col-sm-8">
        <input type="file" id="inputlaporan">
    </div>
</div>
</div>
<!-- /.box-body -->
<div class="box-footer">
	<button type="submit" class="btn btn-info pull-left" style="margin-left:35px">Simpan</button>
</div>
<!-- /.box-footer -->
</form>
</div>
<!-- /.box -->
</section>
<!-- /.content -->
</div>


