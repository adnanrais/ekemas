<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Form Login</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/')?>dist/css/style.css" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
		integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
	<style type="text/css">
    a{
		color: #32be8f !important;
	}
	.nav-link.active{
		color: #495057 !important
	}
    i { 
      font-family: sans;
      color: orange;
    }
  </style>
</head>

<body>
	<img class="wave" src="<?php echo base_url('assets/')?>dist/img/wave.png">
	<div class="container">
		<div class="img">
			<img src="<?php echo base_url('assets/')?>dist/img/img.svg">
		</div>
		<div class="login-container">
			<form action="index.html">
				<img class="avatar" src="<?php echo base_url('assets/')?>dist/img/UNJ.png">
				<h2>E-Kemas</h2>
				<div class="row" >
					
				<ul class="nav nav-tabs" role="tablist" style="margin-left:15px !important">
					<li class="nav-item">
						<a class="nav-link active" href="#mahasiswa" role="tab" data-toggle="tab" >
							Mahasiswa
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#alumni" role="tab" data-toggle="tab">Alumni</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#organisasi" role="tab" data-toggle="tab">Organisasi</a>
					</li>
				</ul>
				</div>
				

				<!-- Tab panes -->
				<div class="tab-content">
					<br>
					<div role="tabpanel" class="tab-pane active" id="mahasiswa">
						<div class="input-div one">
							<div class="i">
								<i class="fas fa-user"></i>
							</div>
							<div>
								<h5>NIM</h5>
								<input class="input" type="text">
							</div>
						</div>

						<div class="input-div two">
							<div class="i">
								<i class="fas fa-lock"></i>
							</div>
							<div>
								<h5>Kata Sandi</h5>
								<input class="input" type="password">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="alumni">
						<div class="input-div one">
							<div class="i">
								<i class="fas fa-user"></i>
							</div>
							<div>
								<h5>ID Alumni</h5>
								<input class="input" type="text">
							</div>
						</div>

						<div class="input-div two">
							<div class="i">
								<i class="fas fa-lock"></i>
							</div>
							<div>
								<h5>Kata Sandi</h5>
								<input class="input" type="password">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="organisasi">
						<div class="input-div one">
							<div class="i">
								<i class="fas fa-user"></i>
							</div>
							<div>
								<h5>ID Organisasi</h5>
								<input class="input" type="text">
							</div>
						</div>

						<div class="input-div two">
							<div class="i">
								<i class="fas fa-lock"></i>
							</div>
							<div>
								<h5>Katas Sandi</h5>
								<input class="input" type="password">
							</div>
						</div>
					</div>
				</div>
				<a href="#">Lupa Kata Sandi?</a>
				<div class="alert alert-danger" role="alert">
					Periksa Kembali ID dan Kata Sandi
				</div>
				<input type="submit" class="btn" value="Login">
			</form>
		</div>
	</div>
	<script type="text/javascript" src="<?php echo base_url('assets/')?>dist/js/main.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
	</script>
</body>

</html>
